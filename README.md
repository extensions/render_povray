![POV@Ble Icon Logo](https://extensions.blender.org/media/images/4f/4fbc5f699ed49d078f132d81d7597ea9a943c87914aec1acd67e35b7d475e89c.png)

# POV@Ble Add-on

## 🛈 Overview

<table id="Gallery" cellpadding="0" border="0">
  <tr>
    <td align="center" background="https://hof.povray.org/images/TopMod_StarBall.jpg"><img src="https://hof.povray.org/images/TopMod_StarBall.jpg"
     alt="Shape of a ball made of ornamented entertwined stars from TopMod. UV mapped in Blender. Rendered, in POV-Ray"
     width:100%;
     max-height="275em"
     style="float: left; margin: auto; border-radius: 5%;" /></td>
    <td align="center" background="https://hof.povray.org/images/myrna.jpg"><img src="https://hof.povray.org/images/myrna.jpg"
     alt="Black and white render of glowy costumed Myrna Loy standing figure her back to an old camera"
     width:100%;
     max-height="275em"
     style="float: left; margin: auto; border-radius: 5%;" /></td>
    <td align="center" background="https://i.imgur.com/GornHhk.jpeg"><img src="https://i.imgur.com/GornHhk.jpeg"
     alt="Three giant robots searching with projectors through the night above a waterside hill"
     width:100%;
     max-height="275em"
     style="float: left; margin: auto; border-radius: 5%;" /></td>
    <td align="center" background="https://i.imgur.com/vm0sjXs.png"><img src="https://i.imgur.com/vm0sjXs.png"
     alt="Bender robot character from futurama"
     width:100%;
     max-height="275em"
     style="float: left; margin: auto; 15px; border-radius: 5%;" /></td>
  </tr>
  <tr>
    <td align="center"><small><b>TopMod StarBall</b> by Janet Lowry</small></td>
    <td align="center"><small><b>Tribute to Myrna</b> by Loy Ive</small></td>
    <td align="center"><small><b>Nowhere to Run</b> by Jhu</small></td>
    <td align="center"><small><b>Bender</b> by Jhu</small></td>
  </tr>
</table>


POV is a *Turing complete* language for many rendering engines (such as *POV-Ray*, but also *Uberpov*, *HgPovray*, *Ray-SAR* simulator, etc.)

This Python project was already part of Blender codebase and download before the turning milestone of [version 2.79b](https://download.blender.org/release/Blender2.79/) up to [Blender 4.1 bundled add-ons](https://docs.blender.org/manual/en/4.1/addons/) when the current system of more independant cloud-based extensions was designed. It still allows anyone to create stunning visualizations with ease, using included UI presets or your own assets :

* By importing some directly in the 3D view… While still treating those as POV native primitives whenever possible, so as to leverage the full Scene Description Langage features.
* Or by typing them as new code snippets inside Blender’s text editor (with specific syntax highlighting)

Advantages include going easier on Blender’s interface, to be more generative at rendertime only ; enhancing POV renders with more graphical skills like painting sculpting, compositing, etc. Or invoking some more hardcore software algorythms through UI routines available in Blender’s toolset...

![df3 library](https://wiki.povray.org/uploaded/4/40/BlenderToPovraySmoke.gif)
*(e.g. physics simulations)*

...with their more polished artistic control than their equivalent found in the more abstract and theoretical realm of POV.

The range of action for a Persistence of Vision renderer extends more than many renderering tools between Non Photo Realistic AND physically based spectrums, and for many more geometry primitives other than polygonal meshes… making it an awesome companion to Freestyle as well as a good general use CG tool.

POV@Ble started by providing synergy between POV-Ray and Blender, but other tools and assets may get duct-taped together in the future to enhance user and developer experience.

![POV@Ble Name Logo](https://cdn.masto.host/mastodonart/accounts/headers/109/305/807/040/938/459/original/2dbb10909ed2286c.png)
## 🗁 Installation

1. Download the latest release from the releases page.
2. Open Blender and go to `Edit > Preferences > Add-ons`.
3. Click `Install` and select the downloaded ZIP file.
4. Enable the add-on by checking the box next to `POV@Ble`.
5. You can pick the renderer binary and *include* libraries there or activate post-render sound.

## 🗹 Usage

1. Set render engine to `Persistence of Vision` in *Render Properties* panel.
2. Configure your scene and render settings as needed.
3. Click `Render` to generate your image or hit **F12**.

## 🖉 Contributing

- ### **testing**
    ...and sharing feedback or **nice output images** on [POV newsgroups](https://news.povray.org/digest/images/) / [Blenderartists forums](https://blenderartists.org/tag/povray) / [Mastodon](https://mastodon.art/tags/povray) / [Facebook](https://www.facebook.com/groups/739767156410999/) / [Instagram](http://instagram.com/tags/povray), etc. is already a contribution… 
    
    Thanks for your interest!

- ### **Translating the documentation**
    in your language as you read it would, of course, be very thoughtful. The official POV-Ray docs also lack a few nice illustrations in some chapters.

- ### **Coding**
    This tool is now available as an extension on the [Extensions platform](https://extensions.blender.org/add-ons/povable).

    To build a new version of the extension:
        * `<path_to_blender> -c extension build --source-dir=./source`

    For more information about building extensions refer to the [documentation](https://docs.blender.org/manual/en/dev/extensions/getting_started.html).

    We welcome more proactive and direct contributions! Please follow these guidelines:

    **Here:**
    - Fork the repository and create a new branch for your feature or bugfix.
    - Use the test files available upon request to fix what broke !
    - Ensure your code adheres to Blender Foundations coding standards.
    - Submit a pull request with a detailed description of your changes.

    ---
        
    But also **elsewhere:**

    Send anything relevant anyway :-) …Code or proposals over any channel we may appear on! 
    This project is maintained by *POVαBle* with the *Blender* and *POV-Ray* communities.

## ♨ Funding

*liberapay* : [POVaBle](https://liberapay.com/POVaBle/donate)

## 🖹 License

This extension is licensed under the **GPL3** License - see the [LICENSE page](https://www.gnu.org/licenses/gpl-3.0.html) for details.
